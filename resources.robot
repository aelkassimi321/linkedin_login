*** Settings ***
Library           SeleniumLibrary
Library           DatabaseLibrary
Resource          Variables.robot

*** Keywords ***
Open Web Browser
    # Start a new Chrome browser
    Open browser    ${URL}    ${Browser}

Select Credentials
    # Connect to the database
    Connect To Database    pymysql    ${DB_NAME}    ${DB_USER}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
    # Execute a SQL query to retrieve the email and password from the database
    ${result}=    Query    SELECT email, password FROM EmailAndPassword WHERE id=1
    Log    ${result}
    Set global variable    ${email}    ${result[0][0]}
    Should Be Equal    ${email}    ${result[0][0]}
    Set global variable    ${password}    ${result[0][1]}
    Should Be Equal    ${password}    ${result[0][1]}
