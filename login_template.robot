*** Settings ***
Library           SeleniumLibrary
Resource          Locators.robot
Resource          Resources.robot
Resource          Variables.robot

*** Keywords ***
Open LinkedIn
    Open Web Browser
    Wait until page contains    Sign in

Authentification
    [Arguments]    ${email}    ${password}
    Input Text    ${LoginInputBox}    ${email}
    Input Text    ${PasswordInputBox}    ${password}
    Click element    ${SubmitButton}
